import {Message} from "../message";

export class UploadFileResponse extends Message{
  token: string
  owners: Array<string>
}
