import {Message} from "../message";

export class GroupMessageRequest extends Message {
  groupName: string
}
