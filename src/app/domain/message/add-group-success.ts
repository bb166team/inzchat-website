import {Message} from "../message";

export class AddGroupSuccess extends Message {
  groupName: string
}
