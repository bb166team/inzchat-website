import {Message} from "../message";

export class PushUserToGroup extends Message {
  username: string
  groupName: string
}
