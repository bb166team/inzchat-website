import {Message} from "../message";

export class LogoutResponse extends Message {
  username: string
}
