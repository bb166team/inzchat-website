import { Message } from "../message";

export class SearchRequest extends Message {
    query: string
}