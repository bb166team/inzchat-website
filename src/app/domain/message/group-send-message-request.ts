import { Message } from "../message";

export class GroupSendMessageRequest extends Message {
  content: string
  groupName: string
  type: string
}
