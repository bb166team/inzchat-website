import { Message } from "../message";

export class LoginRequest extends Message {
    username: string
    password: string
}