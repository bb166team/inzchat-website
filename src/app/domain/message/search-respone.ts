import { Message } from "../message";
import { RoomDTO } from "../dto/room-dto";
import { UserDTO } from "../dto/user-dto";

export class SearchResponse extends Message{
    rooms: Array<RoomDTO>
    users: Array<UserDTO>
}
