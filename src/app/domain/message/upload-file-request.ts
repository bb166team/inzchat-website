import {Message} from "../message";

export class UploadFileRequest extends Message {
  groupType: string
  name: string
}
