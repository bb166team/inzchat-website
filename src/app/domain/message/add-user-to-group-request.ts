import {Message} from "../message";

export class AddUserToGroupRequest extends Message{
  groupName: string
}
