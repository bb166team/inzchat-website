import { Message } from "../message";

export class LoginResponse extends Message {
    loginStatus: string
}