import { Message } from "../message";

export class RegisterRequest extends Message {
    username: string
    password: string
    email: string
}