import {Message} from "../message";

export class PrivateMessageRequest extends Message {
  firstUser: string
}
