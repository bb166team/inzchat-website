import {Message} from "../message";

export class DownloadFileResponse extends Message {
  id: number
  token: string
}
