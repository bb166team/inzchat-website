import {Message} from "../message";

export class PrivateSendMessageRequest extends Message {
  receiver: string
  message: string
  type: string
}
