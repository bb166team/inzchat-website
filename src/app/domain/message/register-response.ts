import { Message } from "../message";

export class RegisterResponse extends Message {
    errors: Array<string>
}