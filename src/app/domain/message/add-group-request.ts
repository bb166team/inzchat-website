import {Message} from "../message";

export class AddGroupRequest extends Message {
  groupName: string
}
