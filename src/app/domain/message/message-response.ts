import {Message} from "../message";
import {MessageDTO} from "../dto/message-dto";

export class MessageResponse extends Message {
  groupName: string
  messages: Array<MessageDTO>
  position: string
}
