import {FileDTO} from "./file-dto";

export class MessageDTO {
  author: string
  message: string
  date: string
  fileDto: FileDTO
  messageType: string
}
