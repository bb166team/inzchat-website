import { Injectable } from '@angular/core';
import { Notifiable } from '../interface/notifiable';
import { environment } from '../../environments/environment';
import {Message} from '../domain/message';

@Injectable({
  providedIn: 'root'
})
export class WebsocketService {
  private components: Array<Notifiable> = []
  private websocket: WebSocket

  constructor() { 
    this.websocket = new WebSocket(environment.wsUrl)

    this.websocket.onmessage = msg => {
      let message: Message = JSON.parse(msg.data)
      this.components.forEach(element => {
        if (element.componentName === message.componentName) {
          element.notifyComponent(message)
        }
      });
    }
  }

  register(component: Notifiable) {
    this.components.push(component)
  }

  send(msg: Message) {
    let message = JSON.stringify(msg)
    this.websocket.send(message)
  }
}
