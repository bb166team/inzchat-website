import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import {HttpClient, HttpHeaders, HttpResponse} from "@angular/common/http";
import {environment} from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class FileService {

  constructor(private http: HttpClient) { }

  uploadFile(token: string, file: File, owners: Array<String>): Observable<any> {
    const formData: FormData = new FormData()

    let headers = new HttpHeaders()
    headers = headers.append("Authorization", "Bearer " + token)
    formData.append('file', file, file.name)
    formData.append(
      "owners",
      new Blob([JSON.stringify(owners === undefined ? [] : owners)],
        {type: "application/json"}
      )
    )
    return this.http.post(environment.fileComponentUrl, formData, {headers: headers});
  }

  downloadFile(token: string, id: number) {
    const url: string = environment.fileComponentUrl + '/' + id
    let headers = new HttpHeaders()
    headers = headers.append("Authorization", "Bearer " + token)
    this.http.get(url, {observe: 'response', responseType: 'arraybuffer', headers: headers}).subscribe((rec: HttpResponse<ArrayBuffer>) => {
      let name: string = rec.headers.get('Content-disposition')
      name = name.substr(name.indexOf("filename=") + 9)
      const blob = new Blob([<BlobPart>rec.body], {type: 'application/octet-stream'})
      const url:string  = window.URL.createObjectURL(blob)
      const link = document.createElement('a')
      link.href = url
      link.setAttribute('download', name)
      document.body.appendChild(link)
      link.click()
    })
  }
}
