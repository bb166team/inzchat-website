import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, CanActivate, Router } from '@angular/router';
import { WebsocketService } from './websocket.service';
import { Message } from '../domain/message';
import { Notifiable } from '../interface/notifiable';
import { LoginRequest } from '../domain/message/login-request';
import { LoginResponse } from '../domain/message/login-response';
import { LoginComponent } from '../component/login/login.component';
import { RegisterComponent } from '../component/register/register.component';
import { RegisterRequest } from '../domain/message/register-request';
import { RegisterResponse } from '../domain/message/register-response';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate, Notifiable {
  private authenticated = false

  private loginResult = "LoginResponse"
  private registerResult = "RegisterUserResponse"

  private success = "SUCCESSFUL"
  private failure = "FILED"

  private loginComponent: LoginComponent;

  private registerUserComponent: RegisterComponent;

  constructor(private router: Router, private websocketService: WebsocketService) {
    this.websocketService.register(this)
  }

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    return this.authenticated
  }

  componentName: string = "authGuard"

  notifyComponent(msg: Message) {
    switch (msg.messageType) {
      case this.loginResult:
        this.onLoginResult(<LoginResponse> msg)
        break
      case this.registerResult:
        this.onRegisterResult(<RegisterResponse> msg)
        break
    }
    if (msg.messageType === this.loginResult) {
      this.onLoginResult(<LoginResponse> msg)
    }
  }

  registerLoginComponent(loginComponent: LoginComponent) {
    this.loginComponent = loginComponent;
  }

  registerRegisterUserComponent(registerComponent: RegisterComponent) {
    this.registerUserComponent = registerComponent;
  }

  private onLoginResult(msg: LoginResponse) {
    switch(msg.loginStatus) {
      case this.success: 
        this.authenticated = true
        this.router.navigate(['/app'])
        break
      case this.failure:
        this.loginComponent.onLoginError(msg);
        break
    }
  }

  private onRegisterResult(msg: RegisterResponse) {
    if (msg.errors.length === 0) 
      this.router.navigate(['/'])
    else 
      this.registerUserComponent.onRegisterErrors(msg);
  }

  login(login: string, passwordHash: string) {
    let message: LoginRequest = {
      componentName: this.componentName,
      messageType: "LoginRequest",
      username: login,
      password: passwordHash
    }

    this.websocketService.send(message)
  }

  register(username: string, password: string, email: string) {
    let message: RegisterRequest = {
      componentName: this.componentName,
      messageType: "RegisterUserRequest",
      username: username,
      password: password,
      email: email
    }

    this.websocketService.send(message)
  }
}
