import { Message } from "../domain/message";

export interface Notifiable {
    componentName: string
    notifyComponent(msg: Message)
}
