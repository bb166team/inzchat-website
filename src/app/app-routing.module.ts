import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './component/login/login.component';
import { WorkspaceComponent } from './component/workspace/workspace.component';
import { AuthGuardService } from './service/auth-guard.service';
import { RegisterComponent } from './component/register/register.component';
import {MessageComponent} from "./component/message/message.component";

const routes: Routes = [
  {path: '', component: LoginComponent},
  {path: 'register', component: RegisterComponent},
  {path: 'app', component: WorkspaceComponent, canActivate: [AuthGuardService], children:
      [{path:':type/:id', component: MessageComponent}]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
