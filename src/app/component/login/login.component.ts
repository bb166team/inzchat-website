import { Component, OnInit } from '@angular/core';
import { AuthGuardService } from 'src/app/service/auth-guard.service';
import { LoginResponse } from 'src/app/domain/message/login-response';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit{
  login: string
  password: string
  error: boolean

  constructor(private authGuard: AuthGuardService) {
    authGuard.registerLoginComponent(this);
  }

  ngOnInit() {
  }

  onLoginError(loginResponse: LoginResponse) {
    this.error = true
  }

  onLogin() {
    this.error = false
    this.authGuard.login(this.login, this.password);
  }
}
