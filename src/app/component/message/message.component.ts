import {Component, OnInit, ViewChild} from '@angular/core';
import {MessageListComponent} from "../message-list/message-list.component";
import {CreateMessageBoxComponent} from "../create-message-box/create-message-box.component";
import {ActivatedRoute} from "@angular/router";
import {MessageDTO} from "../../domain/dto/message-dto";
import {Notifiable} from "../../interface/notifiable";
import {Message} from "../../domain/message";
import {MessageResponse} from "../../domain/message/message-response";
import {WebsocketService} from "../../service/websocket.service";
import {GroupMessageRequest} from "../../domain/message/group-message-request";
import {PrivateMessageRequest} from "../../domain/message/private-message-request";
import {MenuComponent} from "../menu/menu.component";

@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.css']
})
export class MessageComponent implements OnInit, Notifiable {
  @ViewChild('list') messageList: MessageListComponent
  @ViewChild('create') createBox: CreateMessageBoxComponent

  private messages: Map<string, Array<MessageDTO>> = new Map
  private actualDisplayId: string

  constructor(private route: ActivatedRoute, private websocketService: WebsocketService) {
    websocketService.register(this)
  }

  ngOnInit() {
    this.route.paramMap.subscribe(param =>
      this.changeContent(param.get('type'), param.get('id'))
    )
  }

  changeContent(type:string, id: string) {
    this.actualDisplayId = id
    this.createBox.setGroupId(type, id)
    if (this.messages.get(id) === undefined) {
      switch (type) {
        case 'group':
          this.websocketService.send(<GroupMessageRequest>{
            groupName: id,
            componentName: this.componentName,
            messageType: "GroupMessageRequest"
          })
          break
        case 'private':
          this.websocketService.send(<PrivateMessageRequest>{
            firstUser: id,
            componentName: this.componentName,
            messageType: "PrivateMessageRequest"
          })
          break
      }

    } else {
      this.messageList.setDisplayMessages(this.messages.get(id))
    }
  }

  componentName: string = "MessageComponent"

  notifyComponent(msg: Message) {
    if (msg.messageType === "MessageResponse") {
      this.addToGroupMessages(<MessageResponse> msg)
    }
  }

  addToGroupMessages(msg: MessageResponse) {
    msg = this.parseFileMessages(msg)
    if (this.messages.get(msg.groupName) === undefined) {
      this.messages.set(msg.groupName, msg.messages)
    } else {
      let array = this.messages.get(msg.groupName)

      switch (msg.position) {
        case 'END':
          msg.messages.forEach(message => array.unshift(message))
          break
        case 'START':

          msg.messages.forEach(message => array.push(message))
          break
      }
    }

    if (this.actualDisplayId === msg.groupName) {
      this.messageList.setDisplayMessages(this.messages.get(msg.groupName))
    }
  }

  parseFileMessages(msg: MessageResponse) {
    msg.messages = msg.messages.map(message => {
      if (message.messageType === 'FILE') {
        message.fileDto = JSON.parse(message.message)
        message.message = ''
      }
      return message
    })
    return msg
  }
}
