import {Component, OnInit} from '@angular/core';
import { WebsocketService } from 'src/app/service/websocket.service';
import { GroupSendMessageRequest } from 'src/app/domain/message/group-send-message-request';
import {PrivateSendMessageRequest} from "../../domain/message/private-send-message-request";
import {Notifiable} from "../../interface/notifiable";
import {Message} from "../../domain/message";
import {UploadFileRequest} from "../../domain/message/upload-file-request";
import {UploadFileResponse} from "../../domain/message/upload-file-response";
import {FileService} from "../../service/file.service";

@Component({
  selector: 'app-create-message-box',
  templateUrl: './create-message-box.component.html',
  styleUrls: ['./create-message-box.component.css']
})
export class CreateMessageBoxComponent implements OnInit, Notifiable {
  content: string
  private id: string
  private type: string
  private lastUpload: File


  constructor(private websocketService: WebsocketService, private fileService: FileService) {
    websocketService.register(this)
  }

  ngOnInit() {
  }

  componentName: string = "CreateMessageBoxComponent";

  notifyComponent(msg: Message) {
    if (msg.messageType === "UploadFileResponse") {
      const uploadResponse: UploadFileResponse = <UploadFileResponse> msg
      this.fileService.uploadFile(uploadResponse.token, this.lastUpload, uploadResponse.owners).subscribe(m => {
        switch(this.type) {
          case 'group':
            this.websocketService.send(<GroupSendMessageRequest>
              {
                content: JSON.stringify(m),
                componentName: "CreateMessageBoxComponent",
                messageType: "GroupSendMessageRequest",
                groupName: this.id,
                type: 'FILE'
              })
          break
          case 'private':
            this.websocketService.send(<PrivateSendMessageRequest> {
              message: JSON.stringify(m),
              componentName: "CreateMessageBoxComponent",
              messageType: "PrivateSendMessageRequest",
              receiver: this.id,
              type: 'FILE'
            })
          break
        }
      })
    }
  }

  upload(file: File) {
    this.lastUpload = file
    this.websocketService.send(<UploadFileRequest> {
      name: this.id,
      groupType: this.type,
      messageType: "UploadFileRequest",
      componentName: this.componentName,
    })
  }

  setGroupId(type: string, id: string) {
    this.id = id
    this.type = type
  }

  onClick() {
    switch (this.type) {
      case 'group':
        this.websocketService.send(<GroupSendMessageRequest>
          {
            content: this.content,
            componentName: "CreateMessageBoxComponent",
            messageType: "GroupSendMessageRequest",
            groupName: this.id,
            type: 'TEXT'
          })
        this.content = ''
        break
      case 'private':
        this.websocketService.send(<PrivateSendMessageRequest> {
          message: this.content,
          componentName: "CreateMessageBoxComponent",
          messageType: "PrivateSendMessageRequest",
          receiver: this.id,
          type: 'TEXT'
        })
        this.content = ''
        break
    }
  }
}
