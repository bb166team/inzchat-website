import { Component, OnInit } from '@angular/core';
import { AuthGuardService } from 'src/app/service/auth-guard.service';
import { RegisterResponse } from 'src/app/domain/message/register-response';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  login: string
  password: string
  email: string
  errors = false
  private errorsString: Array<string>

  constructor(private auth: AuthGuardService) {
    this.auth.registerRegisterUserComponent(this)
  }

  ngOnInit() {
  }

  onRegister() {
    this.auth.register(this.login, this.password, this.email)
    this.errors = false
  }

  onRegisterErrors(msg:  RegisterResponse) {
    this.errorsString = msg.errors;
    this.errors = true
  }
}
