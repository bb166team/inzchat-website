import {Component, OnInit, ViewChild} from '@angular/core';
import { Notifiable } from 'src/app/interface/notifiable';
import { Message } from 'src/app/domain/message';
import { WebsocketService } from 'src/app/service/websocket.service';
import { SearchRequest } from 'src/app/domain/message/search-request';
import { SearchResponse } from 'src/app/domain/message/search-respone';
import { RefreshMenuRequest } from 'src/app/domain/message/refresh-menu-request';
import {RoomDTO} from "../../domain/dto/room-dto";
import {UserDTO} from "../../domain/dto/user-dto";
import {AddUserToGroupRequest} from "../../domain/message/add-user-to-group-request";
import {NotifyUser} from "../../domain/message/notify-user";
import {LogoutResponse} from "../../domain/message/logout-response";
import {RegisterPrivateUser} from "../../domain/message/register-private-user";
import {AddGroupSuccess} from "../../domain/message/add-group-success";
import {NotifyGroup} from "../../domain/message/notify-group";

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit, Notifiable {

  addGroupVisible = false
  addGroupNotVisible = true

  addUserGroupVisible = false
  addUserGroupNotVisible = true

  groupsEmpty: boolean = true
  groups: Array<RoomDTO> = []

  usersEmpty: boolean = true
  users: Array<UserDTO> = []

  groupType: string
  selectedGroup: string

  constructor(private websocketService: WebsocketService) {
    websocketService.register(this)
  }

  ngOnInit() {
  }

  componentName: string = 'MenuComponent'

  saveGroup(name: string) {
    const message: AddUserToGroupRequest = {
      groupName: name,
      componentName: this.componentName,
      messageType: "AddUserToGroupRequest"
    }
    this.selectedGroup = name
    this.groupType = 'group'
    this.websocketService.send(message)
  }

  changeToPrivate(name: string) {
    this.groupType = 'private'
  }

  notifyComponent(msg: Message) {
    switch(msg.messageType) {
      case "SearchResponse":
        const searchResponse: SearchResponse = <SearchResponse> msg

        if (searchResponse.rooms.length === 0) {
          this.groupsEmpty = true
          this.groups = []
        } else {
          this.groupsEmpty = false
          this.groups = searchResponse.rooms
        }

        if (searchResponse.users.length === 0) {
          this.usersEmpty = true
          this.users = []
        } else {
          this.usersEmpty = false
          this.users = searchResponse.users
        }
        break
      case "NotifyUser":
        const notifyUser: NotifyUser = <NotifyUser> msg
        if (this.users.find(e => e.username === notifyUser.username) === undefined) {
          let userDTO: UserDTO = {
            username: notifyUser.username,
            active: true
          }
          this.users.push(userDTO)
          this.usersEmpty = false
        }
        break
      case "LogoutResponse":
        const logoutResponse: LogoutResponse = <LogoutResponse> msg
        this.users
          .filter(element => logoutResponse.username === element.username)
          .forEach(element => element.active = false)
        break
      case "RegisterPrivateUser":
        const registerPrivateUser: RegisterPrivateUser = <RegisterPrivateUser> msg
        this.users
          .filter(element => registerPrivateUser.username === element.username)
          .forEach(element => element.active = true)
      break
      case "AddGroupSuccess":
        const addGroupSuccess: AddGroupSuccess = <AddGroupSuccess> msg
        this.groups.push({
          name: addGroupSuccess.groupName
        })
      break
      case "NotifyGroup":
        const notifyGroup: NotifyGroup = <NotifyGroup> msg
        this.groups.push({
          name: notifyGroup.groupName
        })
      break
    }
  }

  onSearchChange(query: string) {
    if (query.length > 2) {
      let searchMessage: SearchRequest = {
        query: query,
        componentName: this.componentName,
        messageType: "SearchRequest"
      };

      this.websocketService.send(searchMessage)
    } else if (query.length == 0) {
      let msg: RefreshMenuRequest = {
        componentName: this.componentName,
        messageType: 'RefreshMenuRequest'
      }

      this.websocketService.send(msg)
    } else {
      this.groupsEmpty = true
      this.usersEmpty= true
      this.users = []
      this.groups = []
    }
  }

  addGroup() {
    this.addGroupVisible = true
    this.addGroupNotVisible = false
  }

  addUserToGroup() {
    this.addUserGroupVisible = true
    this.addUserGroupNotVisible = false
  }
}
