import { Component, OnInit } from '@angular/core';
import {MenuComponent} from "../menu/menu.component";
import {WebsocketService} from "../../service/websocket.service";
import {AddGroupRequest} from "../../domain/message/add-group-request";

@Component({
  selector: 'app-add-group',
  templateUrl: './add-group.component.html',
  styleUrls: ['./add-group.component.css']
})
export class AddGroupComponent implements OnInit {
  groupName: string

  constructor(private menu: MenuComponent, private websocket: WebsocketService) { }

  ngOnInit() {
  }

  addGroup() {
    const msg: AddGroupRequest = {
      messageType: "AddGroupRequest",
      componentName: "AddGroupComponent",
      groupName: this.groupName
    }

    this.websocket.send(msg)
    this.menu.addGroupNotVisible = true;
    this.menu.addGroupVisible = false;
  }

  close() {
    this.menu.addGroupNotVisible = true;
    this.menu.addGroupVisible = false;
  }

}
