import { Component, OnInit } from '@angular/core';
import {MenuComponent} from "../menu/menu.component";
import {WebsocketService} from "../../service/websocket.service";
import {PushUserToGroup} from "../../domain/message/push-user-to-group";

@Component({
  selector: 'app-add-user-to-group',
  templateUrl: './add-user-to-group.component.html',
  styleUrls: ['./add-user-to-group.component.css']
})
export class AddUserToGroupComponent implements OnInit {
  username: string

  constructor(private menu: MenuComponent, private websocket: WebsocketService) {}

  ngOnInit() {
  }

  addUserToGroup() {
    const pushUserToGroup: PushUserToGroup = {
      messageType: "PushUserToGroup",
      componentName: "AddUserToGroupComponent",
      groupName: this.menu.selectedGroup,
      username: this.username
    }

    this.websocket.send(pushUserToGroup)
    this.menu.addUserGroupNotVisible = true;
    this.menu.addUserGroupVisible = false;
  }

  close() {
    this.menu.addUserGroupNotVisible = true;
    this.menu.addUserGroupVisible = false;
  }
}
