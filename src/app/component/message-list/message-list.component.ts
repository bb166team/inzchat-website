import { Component, OnInit } from '@angular/core';
import {MessageDTO} from "../../domain/dto/message-dto";
import {WebsocketService} from "../../service/websocket.service";
import {FileService} from "../../service/file.service";
import {DownloadFileRequest} from "../../domain/message/download-file-request";
import {Notifiable} from "../../interface/notifiable";
import {Message} from "../../domain/message";
import {DownloadFileResponse} from "../../domain/message/download-file-response";

@Component({
  selector: 'app-message-list',
  templateUrl: './message-list.component.html',
  styleUrls: ['./message-list.component.css']
})
export class MessageListComponent implements OnInit, Notifiable {
  displayMessages: Array<MessageDTO>

  constructor(private websocketService:  WebsocketService, private fileService: FileService) {
    websocketService.register(this)
  }

  ngOnInit() {
  }

  setDisplayMessages(msg: Array<MessageDTO>) {
    this.displayMessages = msg
  }

  componentName: string = "MessageListComponent";

  notifyComponent(msg: Message) {
    if (msg.messageType === "DownloadFileResponse") {
      const downloadMsg: DownloadFileResponse = <DownloadFileResponse> msg
      this.fileService.downloadFile(downloadMsg.token, downloadMsg.id)
    }
  }

  download(id: number) {
    this.websocketService.send(<DownloadFileRequest> {
      messageType: "DownloadFileRequest",
      componentName: "MessageListComponent",
      id: id
    })
  }
}
