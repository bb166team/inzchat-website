import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpClientModule} from "@angular/common/http";
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MessageListComponent } from './component/message-list/message-list.component';
import { CreateMessageBoxComponent } from './component/create-message-box/create-message-box.component';
import { LoginComponent } from './component/login/login.component';
import { WorkspaceComponent } from './component/workspace/workspace.component';
import { FormsModule } from '@angular/forms';
import { RegisterComponent } from './component/register/register.component';
import { MenuComponent } from './component/menu/menu.component';
import { MessageComponent } from './component/message/message.component';
import { AddGroupComponent } from './component/add-group/add-group.component';
import { AddUserToGroupComponent } from './component/add-user-to-group/add-user-to-group.component';

@NgModule({
  declarations: [
    AppComponent,
    MessageListComponent,
    CreateMessageBoxComponent,
    LoginComponent,
    WorkspaceComponent,
    RegisterComponent,
    MenuComponent,
    MessageComponent,
    AddGroupComponent,
    AddUserToGroupComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
