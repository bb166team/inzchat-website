export const environment = {
  production: true,
  wsUrl: "ws://localhost:9001",
  fileComponentUrl: "http://localhost:8082/file"
};
